package it.piotr.drinkforyou.viewmodel;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import it.piotr.drinkforyou.DrinkRepository;
import it.piotr.drinkforyou.model.Drink;

public class CocktailViewModel extends ViewModel {
    /*isLoading, wentError e isRefreshing valori di utilità per mostrare eventi in UI
        Come start di errore
     */
    private final static String TAG = "CocktailViewModel";
    private MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    private MutableLiveData<Boolean> wentError = new MutableLiveData<>(false);

    private MutableLiveData<List<Drink>> cocktailList;
    private MutableLiveData<Boolean> isRefreshing = new MutableLiveData<>(false);
    private MutableLiveData<Drink> cocktail;
    private final DrinkRepository repository = DrinkRepository.getInstance();
    private static final String CAT_COCKTAIL = "Cocktail";


    public CocktailViewModel() {
        super();
        isLoading.setValue(true);
        cocktailList = repository.getCocktails(CAT_COCKTAIL);
        //mWorkManager = WorkManager.getInstance(app.getApplicationContext());
    }

    @Override
    protected void onCleared() {
        super.onCleared();
    }


    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public MutableLiveData<Boolean> getIsRefreshing() {
        return isRefreshing;
    }

    public void reloadCocktailList() {
        cocktailList.postValue(repository.getCocktails(CAT_COCKTAIL).getValue());
    }

    public MutableLiveData<Boolean> getWentError() {
        return repository.getWentError();
    }

    public MutableLiveData<Boolean> getWentErrorForDetail() {
        return repository.getWentErrorDetail();
    }

    public MutableLiveData<Drink> reloadCocktail(String id) {
        return getCocktail(id);
    }

    public MutableLiveData<Drink> getCocktail(String id) {
        return repository.getSearchedCocktail(id);
    }

    public LiveData<List<Drink>> getCocktailList() {
        return cocktailList;
    }


}