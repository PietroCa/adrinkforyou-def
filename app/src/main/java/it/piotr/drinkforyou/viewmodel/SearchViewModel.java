package it.piotr.drinkforyou.viewmodel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;
import java.util.stream.Collectors;

import it.piotr.drinkforyou.model.Drink;

public class SearchViewModel extends ViewModel {
    private final static String TAG = "SearchViewModel";
    private MutableLiveData<List<Drink>> drinkListFiltered;
    private static final MutableLiveData<String> searchedString = new MutableLiveData<>("");


    public void setSearchedString(String s) {
        searchedString.setValue(s);
    }


    public LiveData<String> getSearchedString() {
        return searchedString;
    }

    public LiveData<List<Drink>> getSearchedDrinks(String s, List<Drink> drinks) {
        if (drinkListFiltered == null) {
            drinkListFiltered = new MutableLiveData<List<Drink>>();
            loadFilteredDrinks(s, drinks);
        }
        return drinkListFiltered;
    }

    private void loadFilteredDrinks(String s, List<Drink> drinks) {
        if (drinks != null) {
            Log.d(TAG, "String s " + s + " size drinks " + drinks.size());
            drinkListFiltered.setValue(drinks.stream().filter(drink -> drink.getDrinkName().toLowerCase().contains(s)).collect(Collectors.toList()));
        }
    }
}
