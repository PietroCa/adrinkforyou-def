package it.piotr.drinkforyou.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import it.piotr.drinkforyou.DrinkRepository;
import it.piotr.drinkforyou.model.Drink;

public class OrdinaryViewModel extends ViewModel {
    /*isLoading, wentError e isRefreshing valori di utilità per mostrare eventi in UI
        Come start di errore
     */
    private final static String TAG = "OrdinaryViewModel";
    private MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    private MutableLiveData<Boolean> wentError = new MutableLiveData<>(false);
    private MutableLiveData<Boolean> isRefreshing = new MutableLiveData<>(false);
    private MutableLiveData<List<Drink>> ordinaryList;
    private final DrinkRepository repository = DrinkRepository.getInstance();
    private static final String CAT_ORDINARY = "Ordinary_Drink";

    public OrdinaryViewModel() {
        super();
        isLoading.setValue(true);
        ordinaryList = repository.getOrdinaries(CAT_ORDINARY);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
    }

    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public LiveData<List<Drink>> getOrdinaries() {
        return ordinaryList;
    }

    public MutableLiveData<Boolean> getWentError() {
        return repository.getWentError();
    }

    public MutableLiveData<Boolean> getIsRefreshing() {
        return isRefreshing;
    }

    public void reloadOrdinaryList() {
        ordinaryList.postValue(repository.getOrdinaries(CAT_ORDINARY).getValue());
    }

}