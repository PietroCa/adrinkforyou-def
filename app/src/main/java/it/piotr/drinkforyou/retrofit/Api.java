package it.piotr.drinkforyou.retrofit;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {
    //Classe Retrofit per l'interfacciamento con l'API

    String BASE_URL = "https://thecocktaildb.com/api/json/v1/1/";

    @Documented
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Retry { //Logiche di retry in caso di errori di rete
        int max() default 3;
    }

    @Retry
    @GET("filter.php")
    Call<ServerResponse> getDrinksByCategory(@Query("c") String category);

    @Retry
    @GET("lookup.php")
    Call<ServerResponse> getDrinkDetailsById(@Query("i") String id);

}
