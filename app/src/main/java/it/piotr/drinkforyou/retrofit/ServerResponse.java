package it.piotr.drinkforyou.retrofit;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import it.piotr.drinkforyou.model.Drink;

public class ServerResponse {
    //Classe che mappa la risposta del server che è di tipo { drinks : [] }
    @SerializedName("drinks")
    private List<Drink> drinkList;

    public List<Drink> getDrinkList() {
        return drinkList;
    }

    public void setDrinkList(List<Drink> drinkList) {
        this.drinkList = drinkList;
    }
}
