package it.piotr.drinkforyou;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import java.util.List;

import it.piotr.drinkforyou.model.Drink;
import it.piotr.drinkforyou.retrofit.Api;
import it.piotr.drinkforyou.retrofit.RetryCallAdapterFactory;
import it.piotr.drinkforyou.retrofit.ServerResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DrinkRepository {
    //Classe repository per l'interfaccia tra viewModel e Retrofit
    private static final String TAG = "DrinkRepository";
    private static DrinkRepository instance;
    private Api api;

    private MutableLiveData<List<Drink>> cocktailList;
    private MutableLiveData<Drink> cocktail;
    private MutableLiveData<Boolean> wentError = new MutableLiveData<>(false);
    private MutableLiveData<Boolean> wentErrorDetail = new MutableLiveData<>(false);
    private MutableLiveData<Boolean> isRefreshing = new MutableLiveData<>(false);

    private static final String CAT_COCKTAIL = "Cocktail";
    private static Retrofit retrofit;
    private MutableLiveData<List<Drink>> ordinaryList;
    private static final String CAT_ORDINARY = "Ordinary_Drink";

    public static DrinkRepository getInstance() {
        if (instance == null) {
            instance = new DrinkRepository();
        }
        return instance;
    }

    public MutableLiveData<Boolean> getWentError() {
        return wentError;
    }

    public MutableLiveData<Boolean> getWentErrorDetail() {
        return wentErrorDetail;
    }

    private DrinkRepository() {
        retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RetryCallAdapterFactory.create())
                .build();
        api = retrofit.create(Api.class);
    }

    public MutableLiveData<List<Drink>> getCocktails(String category) {
        if (cocktailList == null || cocktailList.getValue() == null) {
            cocktailList = new MutableLiveData<List<Drink>>();
            Call<ServerResponse> call = api.getDrinksByCategory(category);

            call.enqueue(new Callback<ServerResponse>() {
                @Override
                public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                    if (response.body() != null) {
                        for (Drink d : response.body().getDrinkList()) {
                            d.setStrCategory(category);
                        }
                        cocktailList.setValue(response.body().getDrinkList());
                        wentError.setValue(false);
                    }
                }

                @Override
                public void onFailure(Call<ServerResponse> call, Throwable t) {
                    Log.e(TAG, "error " + t.getMessage());
                    //In caso di errore setto il valore di errore nel mio viewModel
                    //In ascolto su questo valore al suo cambiamento a true mostro dialog di errore in UI
                    wentError.setValue(true);
                }

            });
        } else {

            if (isRefreshing.getValue()) {
                //Reload della lista fatto tramite SwipeRefreshLayout
                cocktailList.getValue().clear();
                getCocktails(CAT_COCKTAIL);
                Log.d(TAG, "alredy loaded");
            }

        }
        return cocktailList;
    }

    public MutableLiveData<List<Drink>> getOrdinaries(String category) {
        if (ordinaryList == null || ordinaryList.getValue() == null) {
            ordinaryList = new MutableLiveData<List<Drink>>();
            Call<ServerResponse> call = api.getDrinksByCategory(category);

            call.enqueue(new Callback<ServerResponse>() {
                @Override
                public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                    if (response.body() != null) {
                        for (Drink d : response.body().getDrinkList()) {
                            d.setStrCategory(category);
                        }
                        ordinaryList.setValue(response.body().getDrinkList());
                        wentError.setValue(false);
                    }
                }

                @Override
                public void onFailure(Call<ServerResponse> call, Throwable t) {
                    Log.e(TAG, "error " + t.getMessage());
                    ordinaryList.setValue(null);
                    wentError.setValue(true);
                    //In caso di errore setto il valore di errore nel mio viewModel
                    //In ascolto su questo valore al suo cambiamento a true mostro dialog di errore in UI
                }

            });
        } else {
            if (isRefreshing.getValue()) {
                ordinaryList.getValue().clear();
                //Reload della lista fatto tramite SwipeRefreshLayout
                getOrdinaries(CAT_ORDINARY);
                Log.d(TAG, "alredy loaded");
            }
        }
        return ordinaryList;
    }

    public MutableLiveData<Drink> getSearchedCocktail(String id) {

        cocktail = new MutableLiveData<Drink>();
        loadCocktailDetails(id);

        return cocktail;
    }

    private void loadCocktailDetails(String id) {
        Call<ServerResponse> call = api.getDrinkDetailsById(id);

        call.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                if (response.body() != null) {
                    cocktail.setValue(response.body().getDrinkList().get(0)); //Il formato JSON della response utilizza un array di un solo elemento
                }
                wentErrorDetail.setValue(false);
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                Log.e(TAG, "error " + t.getMessage());
                wentErrorDetail.setValue(true);
            }

        });
    }
}
