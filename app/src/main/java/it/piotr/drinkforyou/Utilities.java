package it.piotr.drinkforyou;

import android.content.Context;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

public class Utilities {
    //Classe di utilità con metodi statici

    public static void showErrorDialog(Context context, String title, String message) {
        MaterialAlertDialogBuilder alertDialogBuilder = (MaterialAlertDialogBuilder) new MaterialAlertDialogBuilder(context)
                .setCancelable(true)
                .setMessage(message)
                .setTitle(title);
        alertDialogBuilder.create();
        alertDialogBuilder.show();
    }
}
