package it.piotr.drinkforyou.view;

import android.app.SearchManager;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.Objects;

import it.piotr.drinkforyou.R;
import it.piotr.drinkforyou.viewmodel.SearchViewModel;

public class MainActivity extends AppCompatActivity {
    private final static String TAG = "MainActivity";
    private ContentLoadingProgressBar contentLoadingProgressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Creo la UI
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO); //Forzo la light mode finchè la dark non sarà disponibile
        TabLayout tabLayout = findViewById(R.id.tabLayoutDrink);
        ViewPager viewPager = findViewById(R.id.drinkViewPager);
        contentLoadingProgressBar = findViewById(R.id.progressBarLoading);
        Toolbar toolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        ArrayList<Fragment> fragments = new ArrayList<Fragment>();

        try {
            //Instanzio i fragment per il tabLayout
            fragments.add(getSupportFragmentManager().getFragmentFactory().instantiate(getClassLoader(), OrdinaryFragment.class.getName()));
            fragments.add(getSupportFragmentManager().getFragmentFactory().instantiate(getClassLoader(), CocktailFragment.class.getName()));
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
        }
        CustomPagerAdapter customPagerAdapter = new CustomPagerAdapter(getSupportFragmentManager(), 0, fragments);
        viewPager.setAdapter(customPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        try {

            Objects.requireNonNull(tabLayout.getTabAt(0)).setIcon(R.drawable.ic_baseline_local_drink_24);
            Objects.requireNonNull(tabLayout.getTabAt(0)).setText(getString(R.string.ordinary));
            Objects.requireNonNull(tabLayout.getTabAt(1)).setIcon(R.drawable.ic_baseline_local_bar_24);
            Objects.requireNonNull(tabLayout.getTabAt(1)).setText(getString(R.string.cocktail));
            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    changeColorActivated(tab, R.color.tabColorActivated);
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {
                    changeColorActivated(tab, R.color.white);
                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {
                }
            });
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        tabLayout.selectTab(tabLayout.getTabAt(0));
        viewPager.setCurrentItem(0);
        changeColorActivated(Objects.requireNonNull(tabLayout.getTabAt(0)), R.color.tabColorActivated);
    }

    private void changeColorActivated(TabLayout.Tab tab, int p) {
        int tabIconColor = getColor(p);
        Objects.requireNonNull(tab.getIcon()).setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_actions, menu);
        MenuItem menuItemSearch = menu.findItem(R.id.action_search);
        prepareSearch(menuItemSearch);

        return true;
    }

    @Override
    public void onConfigurationChanged(@NonNull @org.jetbrains.annotations.NotNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getViewModelStore().clear();
    }

    private void prepareSearch(MenuItem itemSearch) {
        SearchViewModel searchViewModel = new ViewModelProvider(this).get(SearchViewModel.class);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) itemSearch.getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            //Ricerco la stringa e sugli eventi sotto setto il MutableLiveData
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.d(MainActivity.this.getClass().getName(), "Submit " + query);
                searchViewModel.setSearchedString(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchViewModel.setSearchedString(newText);
                return false;
            }
        });
    }

    public ContentLoadingProgressBar getContentLoadingProgressBar() {
        return contentLoadingProgressBar;
    }
}