package it.piotr.drinkforyou.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import it.piotr.drinkforyou.R;
import it.piotr.drinkforyou.Utilities;
import it.piotr.drinkforyou.model.Drink;
import it.piotr.drinkforyou.viewmodel.OrdinaryViewModel;
import it.piotr.drinkforyou.viewmodel.SearchViewModel;

public class OrdinaryFragment extends Fragment {
    private final static String TAG = "Ordinary Fragment";
    private OrdinaryViewModel mViewModel;
    private SearchViewModel searchViewModel;
    private RecyclerView recyclerViewOrdinary;
    private List<Drink> ordinaries = new ArrayList<>();
    private DrinkAdapter adapter;
    private View convertView;
    private SwipeRefreshLayout swipeRefreshLayoutOrdinaries;

    public static OrdinaryFragment newInstance() {
        return new OrdinaryFragment();
    }

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        convertView = inflater.inflate(R.layout.ordinary_fragment, container, false);

        return convertView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //metto in ascolto i viewModel

        mViewModel = new ViewModelProvider(this).get(OrdinaryViewModel.class);
        searchViewModel = new ViewModelProvider(this).get(SearchViewModel.class);

        mViewModel.getIsLoading().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            //Osservo il caricamento completato o meno per chiudere la ProgressBar

            @Override
            public void onChanged(Boolean isLoading) {
                if (isLoading) {
                    ((MainActivity) requireActivity()).getContentLoadingProgressBar().show();
                } else {
                    ((MainActivity) requireActivity()).getContentLoadingProgressBar().hide();
                }
            }
        });

        mViewModel.getOrdinaries().observe(getViewLifecycleOwner(), new Observer<List<Drink>>() {
            @Override
            public void onChanged(List<Drink> drinks) {
                //Riempio la lista dei cocktail
                if (drinks != null) {
                    Log.d(this.getClass().getName(), "drinks size in fragment " + drinks.size());
                    ordinaries.clear();
                    ordinaries.addAll(drinks);
                    if (adapter == null) {
                        adapter = new DrinkAdapter(getContext(), ordinaries);
                        recyclerViewOrdinary.setAdapter(adapter);
                    } else {
                        adapter.notifyDataSetChanged();
                        adapter.notifyAdapterDataSetChanged(ordinaries);
                    }
                    mViewModel.getIsLoading().setValue(false);
                    mViewModel.getIsRefreshing().setValue(false);
                    //Setto a false questi due valori per nasconere le indicazioni di caricamento
                }
            }
        });
        searchViewModel.getSearchedString().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                Log.d(TAG, "s è " + s);
                //Ottengo la stringa ricercata settata dalla SearchView e filtro sull'adapter
                adapter.filter(s);
            }
        });
        mViewModel.getWentError().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean error) {
                if (error) {
                    //Mostro dialog e chiudo progress di caricamento
                    Utilities.showErrorDialog(getContext(), getString(R.string.Network_Error), getString(R.string.Network_Error_description));
                    mViewModel.getWentError().setValue(false);
                    mViewModel.getIsLoading().setValue(false);//Reinizializzo
                }
            }
        });
        mViewModel.getIsRefreshing().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean isRefreshing) {
                //osservo il valore settato dal SwipeRefreshLayout

                if (isRefreshing) {
                    mViewModel.reloadOrdinaryList();
                    swipeRefreshLayoutOrdinaries.setRefreshing(false);
                }

            }
        });
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //Creazione UI
        ordinaries = new ArrayList<>();
        recyclerViewOrdinary = view.findViewById(R.id.listOrdinaryDrink);
        recyclerViewOrdinary.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerViewOrdinary.setLayoutManager(linearLayoutManager);
        adapter = new DrinkAdapter(getContext(), ordinaries);
        recyclerViewOrdinary.setAdapter(adapter);
        swipeRefreshLayoutOrdinaries = view.findViewById(R.id.swipeForRefreshOrdinaries);
        swipeRefreshLayoutOrdinaries.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            //Sul refresh setto a true il valore del MutableLiveData
            @Override
            public void onRefresh() {
                mViewModel.getIsRefreshing().setValue(true);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getViewModelStore().clear();
        //Pulizia dei viewModel in ascolto dalla memoria
    }
}