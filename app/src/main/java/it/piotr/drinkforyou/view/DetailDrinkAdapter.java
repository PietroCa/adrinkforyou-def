package it.piotr.drinkforyou.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

import it.piotr.drinkforyou.R;

public class DetailDrinkAdapter extends RecyclerView.Adapter<DetailDrinkAdapter.DetailDrinkViewHolder> {
    //Classe Adapter per la ricetta
    private final Context mContext;
    private final HashMap<String, String> hashMapRecipe;
    private String[] keys;

    public DetailDrinkAdapter(Context mContext, HashMap<String, String> hashMapRecipe) {
        this.mContext = mContext;
        this.hashMapRecipe = hashMapRecipe;
    }

    @NonNull
    @NotNull
    @Override
    public DetailDrinkViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.multiline_text_row_adapter, parent, false);
        return new DetailDrinkViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull DetailDrinkAdapter.DetailDrinkViewHolder holder, int position) {
        keys = hashMapRecipe.keySet().toArray(new String[hashMapRecipe.size()]);
        holder.textViewIngredient.setText(keys[position]);
        holder.textViewMeasure.setText(getItemFromHashmap(position));
    }

    public String getItemFromHashmap(int position) {
        return hashMapRecipe.get(keys[position]);
    }

    @Override
    public int getItemCount() {
        if (hashMapRecipe != null) {
            return hashMapRecipe.size();
        } else {
            return 0;
        }
    }


    static class DetailDrinkViewHolder extends RecyclerView.ViewHolder {
        TextView textViewIngredient;
        TextView textViewMeasure;

        public DetailDrinkViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            textViewIngredient = itemView.findViewById(R.id.ingredientText);
            textViewMeasure = itemView.findViewById(R.id.measureText);
        }

    }
}
