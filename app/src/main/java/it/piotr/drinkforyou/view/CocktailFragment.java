package it.piotr.drinkforyou.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import it.piotr.drinkforyou.R;
import it.piotr.drinkforyou.Utilities;
import it.piotr.drinkforyou.model.Drink;
import it.piotr.drinkforyou.viewmodel.CocktailViewModel;
import it.piotr.drinkforyou.viewmodel.SearchViewModel;

public class CocktailFragment extends Fragment {
    private final static String TAG = "CocktailFragment";
    private CocktailViewModel mViewModel;
    private RecyclerView recyclerViewCocktail;
    private SearchViewModel searchViewModel;
    private List<Drink> cocktails = new ArrayList<>();
    private DrinkAdapter adapter;
    private View convertView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView textViewSwipe;


    public static CocktailFragment newInstance() {
        return new CocktailFragment();
    }

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        convertView = inflater.inflate(R.layout.cocktail_fragment, container, false);

        return convertView;
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //Creo la UI
        swipeRefreshLayout = view.findViewById(R.id.swipeForRefresh);
        cocktails = new ArrayList<>();
        recyclerViewCocktail = view.findViewById(R.id.listCocktailDrink);
        recyclerViewCocktail.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerViewCocktail.setLayoutManager(linearLayoutManager);
        adapter = new DrinkAdapter(getContext(), cocktails);
        recyclerViewCocktail.setAdapter(adapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Sul refresh setto a true il valore del MutableLiveData
                mViewModel.getIsRefreshing().setValue(swipeRefreshLayout.isRefreshing());

            }
        });

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //metto in ascolto i viewModel
        mViewModel = new ViewModelProvider(this).get(CocktailViewModel.class);
        searchViewModel = new ViewModelProvider(this).get(SearchViewModel.class);
        mViewModel.getIsLoading().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean isLoading) {
                //Osservo il caricamento completato o meno per chiudere la ProgressBar
                if (isLoading) {
                    ((MainActivity) requireActivity()).getContentLoadingProgressBar().show();
                } else {
                    ((MainActivity) requireActivity()).getContentLoadingProgressBar().hide();
                }
            }
        });
        mViewModel.getCocktailList().observe(getViewLifecycleOwner(), new Observer<List<Drink>>() {
            @Override
            public void onChanged(List<Drink> drinks) {
                //Riempio la lista dei cocktail
                if (drinks != null) {
                    Log.d(TAG, "drinks size in fragment " + drinks.size());
                    cocktails.clear();
                    cocktails.addAll(drinks);
                    if (adapter == null) {
                        adapter = new DrinkAdapter(getContext(), cocktails);
                        recyclerViewCocktail.setAdapter(adapter);
                    } else {
                        adapter.notifyDataSetChanged();
                        adapter.notifyAdapterDataSetChanged(cocktails);
                    }
                }
                mViewModel.getIsLoading().setValue(false);
                mViewModel.getIsRefreshing().setValue(false);
                //Setto a false questi due valori per nasconere le indicazioni di caricamento
            }
        });
        searchViewModel.getSearchedString().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                //Ottengo la stringa ricercata settata dalla SearchView e filtro sull'adapter
                adapter.filter(s);
            }
        });
        mViewModel.getWentError().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean error) {
                if (error) {
                    //Mostro dialog di errore e chiudo progress di caricamento
                    Utilities.showErrorDialog(getContext(), getString(R.string.Network_Error), getString(R.string.Network_Error_description));
                    mViewModel.getWentError().setValue(false);//Reinizializzo
                    mViewModel.getIsLoading().setValue(false);
                    mViewModel.getIsRefreshing().setValue(false);
                }
            }
        });
        mViewModel.getIsRefreshing().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean isRefreshing) {
                //osservo il valore settato dal SwipeRefreshLayout
                if (isRefreshing) {
                    mViewModel.reloadCocktailList();
                    swipeRefreshLayout.setRefreshing(false);
                }

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getViewModelStore().clear();
        //Pulizia dei viewModel in ascolto dalla memoria
    }
}