package it.piotr.drinkforyou.view;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import it.piotr.drinkforyou.R;
import it.piotr.drinkforyou.model.Drink;

public class DrinkAdapter extends RecyclerView.Adapter<DrinkAdapter.DrinkViewHolder> {
    private static final String TAG = "DrinkAdapter";

    private final Context mContext;
    private final List<Drink> drinkList;
    private final List<Drink> drinkListCopy; //usato per la ricerca nella funzione filter;


    public DrinkAdapter(Context ctx, List<Drink> drinks) {
        this.mContext = ctx;
        this.drinkList = drinks;
        drinkListCopy = new ArrayList<Drink>();
        this.drinkListCopy.addAll(drinks);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        if (drinkList != null) {
            return drinkList.size();
        } else {
            return 0;
        }
    }


    @NonNull
    @NotNull
    @Override
    public DrinkViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_adapter, parent, false);
        return new DrinkViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull DrinkAdapter.DrinkViewHolder holder, int position) {
        Drink drink = drinkList.get(position);
        holder.bind(mContext, drink);
    }

    public void filter(String textFromSearch) {
        drinkList.clear();
        if (textFromSearch.isEmpty()) {
            drinkList.addAll(drinkListCopy);
        } else {
            for (Drink item : drinkListCopy) {
                if (item.getDrinkName().toLowerCase().startsWith(textFromSearch.toLowerCase())) {
                    drinkList.add(item);
                }
            }
        }
        notifyDataSetChanged();
    }

    public void notifyAdapterDataSetChanged(List<Drink> drinks) {
        drinkListCopy.clear();
        drinkListCopy.addAll(drinks);

    }

    static class DrinkViewHolder extends RecyclerView.ViewHolder {
        TextView textViewDrinkName;
        ImageView thumbImageView;
        ImageButton btnDetails;

        public DrinkViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            textViewDrinkName = itemView.findViewById(R.id.drinkName);
            thumbImageView = itemView.findViewById(R.id.thumbImageDrink);
            btnDetails = itemView.findViewById(R.id.btnSeeDetails);
        }

        public void bind(Context mContext, final Drink drinkItem) {
            textViewDrinkName.setText(drinkItem.getDrinkName());
            thumbImageView.setContentDescription(drinkItem.getDrinkName());
            try {
                RequestOptions requestOptions = new RequestOptions().placeholder(R.drawable.ic_placeholder).centerInside();
                Glide.with(mContext).
                        load(drinkItem.getUrlThumbImage()).sizeMultiplier(1.0f).apply(requestOptions)
                        .into(thumbImageView);
                //Uso Glide che prendere per me la URL dell'immagine e la scarica nella mia ImageView
            } catch (Exception e) {
                thumbImageView.setImageResource(R.drawable.ic_placeholder);
            }
            btnDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    seeDetails(mContext, drinkItem);
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    seeDetails(mContext, drinkItem);
                }
            });
        }

        private void seeDetails(Context mContext, Drink drinkItem) {
            //Sul click mando l'intent all'activity di dettaglio
            //Uso il parametro drinkId
            Intent drinkIntent = new Intent(mContext, DetailViewActivity.class);
            drinkIntent.putExtra("id", drinkItem.getIdDrink());
            Log.d(TAG, "DRINK ID " + drinkItem.getIdDrink());
            drinkIntent.putExtra("name", drinkItem.getDrinkName());
            mContext.startActivity(drinkIntent);
        }
    }
}
