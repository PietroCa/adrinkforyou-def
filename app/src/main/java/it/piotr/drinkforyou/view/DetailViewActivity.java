package it.piotr.drinkforyou.view;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;

import it.piotr.drinkforyou.R;
import it.piotr.drinkforyou.Utilities;
import it.piotr.drinkforyou.model.Drink;
import it.piotr.drinkforyou.viewmodel.CocktailViewModel;

public class DetailViewActivity extends AppCompatActivity {
    private final static String TAG = "DetailViewActivity";
    private String drinkName;
    private String drinkId;
    private CocktailViewModel cocktailViewModel;
    private HashMap<String, String> recipe = new HashMap<>();
    private DetailDrinkAdapter detailDrinkAdapter;
    private SwipeRefreshLayout swipeRefreshLayoutDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Creazione UI
        setContentView(R.layout.activity_detail_view);
        Toolbar toolbar = findViewById(R.id.toolbarDetail);
        toolbar.setNavigationIcon(R.drawable.ic_round_arrow_back_ios_24);
        ImageView imageView = findViewById(R.id.imageDrinkDetail);
        TextView textViewInstructions = findViewById(R.id.preparationsContent);
        RecyclerView recyclerView = findViewById(R.id.listIngredients);
        recyclerView.setAdapter(new DetailDrinkAdapter(this, recipe));
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(true);
        swipeRefreshLayoutDetail = findViewById(R.id.swipeForRefreshDetail);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        drinkName = getIntent().getStringExtra("name");
        toolbar.setTitle(drinkName);
        drinkId = getIntent().getStringExtra("id");
        cocktailViewModel = new ViewModelProvider.AndroidViewModelFactory(this.getApplication()).create(CocktailViewModel.class); //new ViewModelProvider(this).get(CocktailViewModel.class);

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        try {
            DividerItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
            itemDecoration.setDrawable(getDrawable(R.drawable.divider_for_list_row));
            recyclerView.addItemDecoration(itemDecoration);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        cocktailViewModel.getCocktail(drinkId).observe(this, new Observer<Drink>() {
            @Override
            public void onChanged(Drink drink) {
                try {
                    RequestOptions requestOptions = new RequestOptions()
                            .placeholder(R.drawable.ic_placeholder).centerCrop();
                    imageView.setContentDescription(drinkName + " image");
                    Glide.with(DetailViewActivity.this).
                            load(drink.getUrlThumbImage()).sizeMultiplier(1.0f).apply(requestOptions)
                            .into(imageView);


                } catch (Exception e) {
                    imageView.setImageResource(R.drawable.ic_placeholder);
                }

                if (Locale.getDefault().getLanguage().equalsIgnoreCase(String.valueOf(Locale.ENGLISH))
                        || Locale.getDefault().getLanguage().equalsIgnoreCase(String.valueOf(Locale.US))) {
                    textViewInstructions.setText(drink.getInstructionsENG());
                } else if (Locale.getDefault().getLanguage().equalsIgnoreCase(String.valueOf(Locale.ITALIAN))) {
                    textViewInstructions.setText(drink.getInstructionsIT());
                }

                try {
                    drink.createRecipe(drink);
                    recipe = drink.getRecipe();
                    Log.d(TAG, "recipe size " + recipe.size());
                    detailDrinkAdapter = new DetailDrinkAdapter(DetailViewActivity.this, recipe);
                    recyclerView.setAdapter(detailDrinkAdapter);

                } catch (InvocationTargetException | IllegalAccessException e) {
                    e.printStackTrace();
                }

            }

        });
        recyclerView.setAdapter(detailDrinkAdapter);
        cocktailViewModel.getWentErrorForDetail().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean error) {
                //Ascolto l'errore per mostrare la dialog
                if (error) {
                    Utilities.showErrorDialog(DetailViewActivity.this, getString(R.string.Network_Error), getString(R.string.Network_Error_description_detail));
                    cocktailViewModel.getWentErrorForDetail().setValue(false);
                }
            }
        });
        swipeRefreshLayoutDetail.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                cocktailViewModel.getIsRefreshing().setValue(swipeRefreshLayoutDetail.isRefreshing());
            }
        });
        cocktailViewModel.getIsRefreshing().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean isRefreshing) {
                if (isRefreshing) {
                    swipeRefreshLayoutDetail.setRefreshing(false);
                    recreate();
                }
            }
        });
    }

    @Override
    public void onConfigurationChanged(@NonNull @NotNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getViewModelStore().clear();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        DetailViewActivity.this.finish();
    }
}