package it.piotr.drinkforyou.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

public class Drink {
    //Creo questa Pojo Class per mappare la risposta del JSON. 
    @SerializedName("strDrink")
    @Expose
    private String drinkName;
    @SerializedName("strDrinkThumb")
    @Expose
    private String urlThumbImage;
    @SerializedName("idDrink")
    @Expose
    private String idDrink;
    @SerializedName("strInstructions")
    @Expose
    private String instructionsENG;
    @SerializedName("strInstructionsIT")
    @Expose
    private String instructionsIT;
    @SerializedName("strInstructionsDE")
    @Expose
    private String instructionsDE;
    @SerializedName("strInstructionsFR")
    @Expose
    private String instructionsFR;
    @SerializedName("strInstructionsES")
    @Expose
    private String instructionsES;

    @SerializedName("strCategory")
    @Expose
    private String strCategory;

    @SerializedName("strImageSource")
    @Expose
    private String imageSrc;
    private String strIngredient1;
    @SerializedName("strIngredient2")
    @Expose
    private String strIngredient2;
    @SerializedName("strIngredient3")
    @Expose
    private String strIngredient3;
    @SerializedName("strIngredient4")
    @Expose
    private String strIngredient4;
    @SerializedName("strIngredient5")
    @Expose
    private String strIngredient5;
    @SerializedName("strIngredient6")
    @Expose
    private String strIngredient6;
    @SerializedName("strIngredient7")
    @Expose
    private String strIngredient7;
    @SerializedName("strIngredient8")
    @Expose
    private String strIngredient8;
    @SerializedName("strIngredient9")
    @Expose
    private String strIngredient9;
    @SerializedName("strIngredient10")
    @Expose
    private String strIngredient10;
    @SerializedName("strIngredient11")
    @Expose
    private String strIngredient11;
    @SerializedName("strIngredient12")
    @Expose
    private String strIngredient12;
    @SerializedName("strIngredient13")
    @Expose
    private String strIngredient13;
    @SerializedName("strIngredient14")
    @Expose
    private String strIngredient14;
    @SerializedName("strIngredient15")
    @Expose
    private String strIngredient15;
    @SerializedName("strMeasure1")
    @Expose
    private String strMeasure1;
    @SerializedName("strMeasure2")
    @Expose
    private String strMeasure2;
    @SerializedName("strMeasure3")
    @Expose
    private String strMeasure3;
    @SerializedName("strMeasure4")
    @Expose
    private String strMeasure4;
    @SerializedName("strMeasure5")
    @Expose
    private String strMeasure5;
    @SerializedName("strMeasure6")
    @Expose
    private String strMeasure6;
    @SerializedName("strMeasure7")
    @Expose
    private String strMeasure7;
    @SerializedName("strMeasure8")
    @Expose
    private String strMeasure8;
    @SerializedName("strMeasure9")
    @Expose
    private String strMeasure9;
    @SerializedName("strMeasure10")
    @Expose
    private String strMeasure10;
    @SerializedName("strMeasure11")
    @Expose
    private String strMeasure11;
    @SerializedName("strMeasure12")
    @Expose
    private String strMeasure12;
    @SerializedName("strMeasure13")
    @Expose
    private String strMeasure13;
    @SerializedName("strMeasure14")
    @Expose
    private String strMeasure14;
    @SerializedName("strMeasure15")
    @Expose
    private String strMeasure15;

    private HashMap<String, String> recipe;

    public Drink(String drinkName, String urlThumbImage, String idDrink) {
        this.drinkName = drinkName;
        this.urlThumbImage = urlThumbImage;
        this.idDrink = idDrink;
    }

    public Drink(String drinkName, String urlThumbImage,
                 String idDrink, String instructionsENG,
                 String instructionsIT, String instructionsDE,
                 String instructionsFR, String instructionsES,
                 String imageSrc) {
        this.drinkName = drinkName;
        this.urlThumbImage = urlThumbImage;
        this.idDrink = idDrink;
        this.instructionsENG = instructionsENG;
        this.instructionsIT = instructionsIT;
        this.instructionsDE = instructionsDE;
        this.instructionsFR = instructionsFR;
        this.instructionsES = instructionsES;
        this.imageSrc = imageSrc;
    }

    public Drink() {

    }

    public HashMap<String, String> getRecipe() {
        return recipe;
    }

    public void createRecipe(Object dstObject) throws InvocationTargetException, IllegalAccessException {
        /*Prendo tutte le proprietà getter della mia classe tramite la reflection per creare
        la lista delle coppie Ingrediente e quantità per la schermata di dettaglio
         */
        try {
            Class c = this.getClass();
            if (recipe == null) {
                recipe = new HashMap<String, String>();
            }
            for (int i = 0; i < c.getDeclaredMethods().length; i++) {
                Method methodIngredient = c.getDeclaredMethod("getStrIngredient" + (i + 1));
                Method methodMeasure = c.getDeclaredMethod("getStrMeasure" + (i + 1));
                methodIngredient.setAccessible(true);
                Object ingredient = methodIngredient.invoke(dstObject);
                Object measure = methodMeasure.invoke(dstObject);
                if (ingredient == null) {
                    ingredient = "";
                }
                if (measure == null) {
                    measure = "";
                }
                if (!ingredient.toString().isEmpty() || !measure.toString().isEmpty()) {
                    recipe.put(ingredient.toString(), measure.toString());
                }
            }
        } catch (NoSuchMethodException e) {
            setRecipe(recipe);
        }
    }


    public String getStrCategory() {
        return strCategory;
    }

    public void setStrCategory(String strCategory) {
        this.strCategory = strCategory;
    }

    public void setRecipe(HashMap<String, String> recipe) {
        this.recipe = recipe;
    }

    public String getDrinkName() {
        return drinkName;
    }

    public void setDrinkName(String drinkName) {
        this.drinkName = drinkName;
    }

    public String getUrlThumbImage() {
        return urlThumbImage;
    }

    public void setUrlThumbImage(String urlThumbImage) {
        this.urlThumbImage = urlThumbImage;
    }

    public String getIdDrink() {
        return idDrink;
    }

    public void setIdDrink(String idDrink) {
        this.idDrink = idDrink;
    }

    public String getInstructionsENG() {
        return instructionsENG;
    }

    public void setInstructionsENG(String instructionsENG) {
        this.instructionsENG = instructionsENG;
    }

    public String getInstructionsIT() {
        return instructionsIT;
    }

    public void setInstructionsIT(String instructionsIT) {
        this.instructionsIT = instructionsIT;
    }

    public String getInstructionsDE() {
        return instructionsDE;
    }

    public void setInstructionsDE(String instructionsDE) {
        this.instructionsDE = instructionsDE;
    }

    public String getInstructionsFR() {
        return instructionsFR;
    }

    public void setInstructionsFR(String instructionsFR) {
        this.instructionsFR = instructionsFR;
    }

    public String getInstructionsES() {
        return instructionsES;
    }

    public void setInstructionsES(String instructionsES) {
        this.instructionsES = instructionsES;
    }

    public String getImageSrc() {
        return imageSrc;
    }

    public void setImageSrc(String imageSrc) {
        this.imageSrc = imageSrc;
    }

    public String getStrIngredient1() {
        return strIngredient1;
    }

    public void setStrIngredient1(String strIngredient1) {
        this.strIngredient1 = strIngredient1;
    }

    public String getStrIngredient2() {
        return strIngredient2;
    }

    public void setStrIngredient2(String strIngredient2) {
        this.strIngredient2 = strIngredient2;
    }

    public String getStrIngredient3() {
        return strIngredient3;
    }

    public void setStrIngredient3(String strIngredient3) {
        this.strIngredient3 = strIngredient3;
    }

    public String getStrIngredient4() {
        return strIngredient4;
    }

    public void setStrIngredient4(String strIngredient4) {
        this.strIngredient4 = strIngredient4;
    }

    public String getStrIngredient5() {
        return strIngredient5;
    }

    public void setStrIngredient5(String strIngredient5) {
        this.strIngredient5 = strIngredient5;
    }

    public String getStrIngredient6() {
        return strIngredient6;
    }

    public void setStrIngredient6(String strIngredient6) {
        this.strIngredient6 = strIngredient6;
    }

    public String getStrIngredient7() {
        return strIngredient7;
    }

    public void setStrIngredient7(String strIngredient7) {
        this.strIngredient7 = strIngredient7;
    }

    public String getStrIngredient8() {
        return strIngredient8;
    }

    public void setStrIngredient8(String strIngredient8) {
        this.strIngredient8 = strIngredient8;
    }

    public String getStrIngredient9() {
        return strIngredient9;
    }

    public void setStrIngredient9(String strIngredient9) {
        this.strIngredient9 = strIngredient9;
    }

    public String getStrIngredient10() {
        return strIngredient10;
    }

    public void setStrIngredient10(String strIngredient10) {
        this.strIngredient10 = strIngredient10;
    }

    public String getStrIngredient11() {
        return strIngredient11;
    }

    public void setStrIngredient11(String strIngredient11) {
        this.strIngredient11 = strIngredient11;
    }

    public String getStrIngredient12() {
        return strIngredient12;
    }

    public void setStrIngredient12(String strIngredient12) {
        this.strIngredient12 = strIngredient12;
    }

    public String getStrIngredient13() {
        return strIngredient13;
    }

    public void setStrIngredient13(String strIngredient13) {
        this.strIngredient13 = strIngredient13;
    }

    public String getStrIngredient14() {
        return strIngredient14;
    }

    public void setStrIngredient14(String strIngredient14) {
        this.strIngredient14 = strIngredient14;
    }

    public String getStrIngredient15() {
        return strIngredient15;
    }

    public void setStrIngredient15(String strIngredient15) {
        this.strIngredient15 = strIngredient15;
    }

    public String getStrMeasure1() {
        return strMeasure1;
    }

    public void setStrMeasure1(String strMeasure1) {
        this.strMeasure1 = strMeasure1;
    }

    public String getStrMeasure2() {
        return strMeasure2;
    }

    public void setStrMeasure2(String strMeasure2) {
        this.strMeasure2 = strMeasure2;
    }

    public String getStrMeasure3() {
        return strMeasure3;
    }

    public void setStrMeasure3(String strMeasure3) {
        this.strMeasure3 = strMeasure3;
    }

    public String getStrMeasure4() {
        return strMeasure4;
    }

    public void setStrMeasure4(String strMeasure4) {
        this.strMeasure4 = strMeasure4;
    }

    public String getStrMeasure5() {
        return strMeasure5;
    }

    public void setStrMeasure5(String strMeasure5) {
        this.strMeasure5 = strMeasure5;
    }

    public String getStrMeasure6() {
        return strMeasure6;
    }

    public void setStrMeasure6(String strMeasure6) {
        this.strMeasure6 = strMeasure6;
    }

    public String getStrMeasure7() {
        return strMeasure7;
    }

    public void setStrMeasure7(String strMeasure7) {
        this.strMeasure7 = strMeasure7;
    }

    public String getStrMeasure8() {
        return strMeasure8;
    }

    public void setStrMeasure8(String strMeasure8) {
        this.strMeasure8 = strMeasure8;
    }

    public String getStrMeasure9() {
        return strMeasure9;
    }

    public void setStrMeasure9(String strMeasure9) {
        this.strMeasure9 = strMeasure9;
    }

    public String getStrMeasure10() {
        return strMeasure10;
    }

    public void setStrMeasure10(String strMeasure10) {
        this.strMeasure10 = strMeasure10;
    }

    public String getStrMeasure11() {
        return strMeasure11;
    }

    public void setStrMeasure11(String strMeasure11) {
        this.strMeasure11 = strMeasure11;
    }

    public String getStrMeasure12() {
        return strMeasure12;
    }

    public void setStrMeasure12(String strMeasure12) {
        this.strMeasure12 = strMeasure12;
    }

    public String getStrMeasure13() {
        return strMeasure13;
    }

    public void setStrMeasure13(String strMeasure13) {
        this.strMeasure13 = strMeasure13;
    }

    public String getStrMeasure14() {
        return strMeasure14;
    }

    public void setStrMeasure14(String strMeasure14) {
        this.strMeasure14 = strMeasure14;
    }

    public String getStrMeasure15() {
        return strMeasure15;
    }

    public void setStrMeasure15(String strMeasure15) {
        this.strMeasure15 = strMeasure15;
    }
}
